﻿using HoloToolkit.Unity;
using System.Collections;
using UnityEngine;
using UnityEngine.Windows.Speech;

public class AstronautManager : MonoBehaviour {

    ARAI_IntelligenEntity aRAI_IntelligenEntity;
    public ARAI_IntelligenManager IntelligentManager;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    /// <summary>
    /// This function is called when you said 'Show me your face'
    /// to the character which is being gazed. The character shows
    /// his 'happy' face and his emotion (jump up).
    /// </summary>
    public void ShowMeFace()
    {
        aRAI_IntelligenEntity.animator.Play("Happy", -1, 0f);
    }

    /// <summary>
    /// ComebackOrginalPosition is called when you said 'Come back'
    /// to the character which is being gazed.
    /// The character will come back its original position.
    /// </summary>
    public void ComebackOrginalPosition()
    {
        var currPos = transform.position;
        //IntelligentManager.MoveToPositionGazedObject(this.gameObject, originalPos);

    }
}
