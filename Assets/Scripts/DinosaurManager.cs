﻿using HoloToolkit.Unity;
using System.Collections;
using UnityEngine;
using UnityEngine.Windows.Speech;

public class DinosaurManager : MonoBehaviour {

    public ARAI_IntelligenManager IntelligentManager;
    GameObject objectToMove = null;

    bool isObjectFocused = false;
    bool isDinoFocused = false;
    bool isMoved = false;
    bool isCollide = false;

    Vector3 originalPos;
    Vector3 currentDinoPos;
    Vector3 currentObjectPos;

    // Use this for initialization
    void Start () {
        originalPos = this.transform.position;
    }


    // Update is called once per frame
    void Update () {

        // Define which object is being gazed 
        if (GazeManager.Instance.Hit && GazeManager.Instance.HitInfo.collider.gameObject.transform.tag == "go")
        {
            isObjectFocused = true;
            currentObjectPos = GazeManager.Instance.HitInfo.collider.gameObject.transform.position;
        }
        else
        {
            isObjectFocused = false;
        }

    }

    /// <summary>
    /// This function is called when you said 'Show me your face'
    /// to the character which is being gazed. The character shows
    /// his 'happy' face and his emotion (jump up).
    /// </summary>
    public void ShowMeFace()
    {
        IntelligentManager.intelligentEntity.animator.Play("Happy", -1, 0f);
    }

    /// <summary>
    /// ComebackOrginalPosition is called when you said 'Come back'
    /// to the character which is being gazed.
    /// The character will come back its original position.
    /// </summary>
    public void ComebackOrginalPosition()
    {
        var currPos = transform.position;
        IntelligentManager.MoveToPositionGazedObject(this.gameObject, originalPos);
        
    }

    /// <summary>
    /// This function is called when you ask the character to get something
    /// in the scene. The character will get any object you are pointing and
    /// come back his original position.
    /// </summary>
    public void GetObject()
    {
        // check the current gazed object
        if (isObjectFocused)
        {
            objectToMove = GazeManager.Instance.HitInfo.collider.gameObject;

            currentDinoPos = this.gameObject.transform.position;

            // move the dino to the current object
            IntelligentManager.MoveToPositionGazedObject(this.gameObject, currentObjectPos);

            // set the dino is moved
            isMoved = true;

            StartCoroutine(MoveDIno());
        }
    }

    /// <summary>
    /// This function allows the character to go to another position
    /// when he is asked with the command 'Go there'. User will gaze
    /// to the character and gaze to another position in the scene.
    /// </summary>
    public void GoSomewhere()
    {
        currentDinoPos = this.gameObject.transform.position;
        IntelligentManager.MoveToPositionGazedObject(this.gameObject, GazeManager.Instance.Position);
    }

    /// <summary>
    /// This function allows the character brings the object when he tries
    /// to get something in the scene. This function is called in the Update().
    /// </summary>
    IEnumerator MoveObject(Transform thisTransform, Vector3 startPos, Vector3 endPos, float time)
    {
        var i = 0.0f;
        var rate = 1.0f / time;
        while (i < 1.0f)
        {
            i += Time.deltaTime * rate;
            thisTransform.position = Vector3.Lerp(startPos, endPos, i);
            yield return null;
        }

    }

    IEnumerator MoveDIno() 
    {
        bool isColided = false;
     
        while (!isColided)
        {
            Vector3 dist = this.gameObject.transform.position - currentObjectPos;

            if (dist.magnitude <= .3)
            {
                isColided = true;
                
            }
            yield return null;
        }

        IntelligentManager.MoveToPositionGazedObject(this.gameObject, currentDinoPos);
        StartCoroutine(MoveObject(objectToMove.transform, objectToMove.transform.position, currentDinoPos, 3.0f));
    }


}
