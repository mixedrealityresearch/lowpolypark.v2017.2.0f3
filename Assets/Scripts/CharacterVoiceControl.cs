﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Windows.Speech;
using System.Collections;

public class CharacterVoiceControl : MonoBehaviour {

    string[] keywords = new string[] { "Move there", "Get the box" ,"Come back" };
    public ConfidenceLevel confidence = ConfidenceLevel.Low;

    //[HideInInspector]
    public float speed = 0.002f;
    Vector3 originalPos;
    Vector3 desPos;

    protected PhraseRecognizer recognizer;
    protected string word = "";

    // Use this for initialization
    void Start () {

        originalPos = this.transform.position;

        if (keywords != null)
        {
            recognizer = new KeywordRecognizer(keywords);
            recognizer.OnPhraseRecognized += Recognizer_OnPhraseRecognized;
            recognizer.Start();
        }
    }

    private void Recognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        word = args.text;
        Debug.Log("CharacterVoiceControl");
        Debug.Log("You said: <b> " + args.text);
    }

    void OnCollisionEnter(Collision collisionInfo)
    {
        Debug.Log("Detected collision between " + gameObject.name + " and " + collisionInfo.collider.name);

        if (collisionInfo.collider.tag == "cube")
        {
            Debug.Log("You hit a cube!");
            //this.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero; // stop moving
        }
    }

    /*void OnCollisionStay(Collision collisionInfo)
    {
        Debug.Log(gameObject.name + " and " + collisionInfo.collider.name + " are still colliding");
    }

    void OnCollisionExit(Collision collisionInfo)
    {
        Debug.Log(gameObject.name + " and " + collisionInfo.collider.name + " are no longer colliding");
    }*/

    void Update()
    {
        //var x = this.transform.position.x;
        //var y = this.transform.position.y;
        //var z = this.transform.position.z;

        bool isObjectFocused = false;
        //this.transform.Rotate(Vector3.left, temp * Time.deltaTime);

        if (GazeGestureManager.Instance.FocusedObject != null)
        {
            if (GazeGestureManager.Instance.FocusedObject.name != "dino" && GazeGestureManager.Instance.FocusedObject.tag != "plane")
            {
                isObjectFocused = true;
                desPos = GazeGestureManager.Instance.FocusedObject.transform.position;

            }
            else
            {
                isObjectFocused = false;
            }
        }

        switch (word)
        {
            case "Move there":

                // check the current gazed object
                if (isObjectFocused)
                {
                    //Debug.Log("You are gazing at: " + GazeGestureManager.Instance.FocusedObject.name + ". Position: " + GazeGestureManager.Instance.FocusedObject.transform.position);
                    //Debug.Log("Position of Dino: " + this.transform.position);

                    var currPos = transform.position;
                    StartCoroutine(MoveObject(transform, currPos, desPos , 3.0f));

                    word = "";
                }
                break;
            case "Get the box":

                // check the current gazed object
                if (isObjectFocused)
                {
                    Debug.Log("You are gazing at: " + GazeGestureManager.Instance.FocusedObject.name + ". Position: " + GazeGestureManager.Instance.FocusedObject.transform.position);
                    Debug.Log("Position of Dino: " + this.transform.position);

                    // move to the object and then comeback at the original Position
                    var currPos = transform.position;

                    StartCoroutine(MoveObject(transform, currPos, desPos, 3.0f));
                    StartCoroutine(MoveObject(transform, desPos, currPos, 3.0f));

                    // The object also moves follow the character
                    StartCoroutine(MoveObject(GazeGestureManager.Instance.FocusedObject.transform, desPos, currPos, 3.0f));

                    /*float distance = Vector3.Distance(currPos, originalPos);
                    if (distance == 0.0)
                    {
                        StartCoroutine(MoveObject(transform, desPos, currPos, 3.0f));
                    }
                    else
                    {
                        StartCoroutine(MoveObject(transform, desPos, originalPos, 3.0f));
                    }

                    // Move the object follow the character
                    StartCoroutine(MoveObject(transform, GazeGestureManager.Instance.FocusedObject.transform.position, transform.position, 3.0f));*/

                    word = "";
                }
                break;
            case "Come back":

                // call the character to comeback the original position
                if (!isObjectFocused)
                {
                    var currPos = transform.position;
                    StartCoroutine(MoveObject(transform, currPos, originalPos, 3.0f));

                    word = "";
                }
                break;
        }

    }

    IEnumerator MoveObject(Transform thisTransform, Vector3 startPos, Vector3 endPos, float time)
    {
        var i = 0.0f;
        var rate = 1.0f / time;
        while (i < 1.0f)
        {
            i += Time.deltaTime * rate;
            thisTransform.position = Vector3.Lerp(startPos, endPos, i);
            yield return null;
        }
    }


    private void OnApplicationQuit()
    {
        if (recognizer != null && recognizer.IsRunning)
        {
            recognizer.OnPhraseRecognized -= Recognizer_OnPhraseRecognized;
            recognizer.Stop();
        }
    }

}
