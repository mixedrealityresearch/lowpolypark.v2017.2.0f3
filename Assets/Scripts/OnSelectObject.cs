﻿using HoloToolkit.Unity;
using UnityEngine;

public class OnSelectObject : MonoBehaviour {

    public ARAI_IntelligenManager IntelligentManager;

    void OnSelect()
    {
        if (GazeManager.Instance.Hit && GazeManager.Instance.HitInfo.collider.gameObject.transform.tag == "dinosaur")
        {
            ARAI_IntelligenEntity intelligentEntity = GazeManager.Instance.HitInfo.collider.gameObject.GetComponent<ARAI_IntelligenEntity>();
            intelligentEntity.animator.Play("Happy", -1, 0f);
                        
            string _name = GazeManager.Instance.HitInfo.collider.gameObject.name;
            _name = _name.Substring(7);
            Debug.Log("NAME: " + _name);

            var st = _name.Split(' ');
            Debug.Log("st: " + st[0]);

            IntelligentManager.intelligentSelectTalkGazedObject(GazeManager.Instance.HitInfo.collider.gameObject,"Hello! My name is " + st[0] + ". Nice to meet you! ");
        }
        
    }
}
