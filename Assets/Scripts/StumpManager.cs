﻿//using Academy.HoloToolkit.Unity;
using HoloToolkit.Unity;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Windows.Speech;

public class StumpManager : Singleton<StumpManager>
{
    // KeywordRecognizer object.
    KeywordRecognizer keywordRecognizer;

    // Defines which function to call when a keyword is recognized.
    delegate void KeywordAction(PhraseRecognizedEventArgs args);
    Dictionary<string, KeywordAction> keywordCollection;

    void Start()
    {
        keywordCollection = new Dictionary<string, KeywordAction>();

        // Add keyword to start moving the cube.
        keywordCollection.Add("Move", MoveCubeCommand);

        // Add keyword to start scaling the cube.
        //keywordCollection.Add("Hello", ScaleCubeCommand);

        // Initialize KeywordRecognizer with the previously added keywords.
        keywordRecognizer = new KeywordRecognizer(keywordCollection.Keys.ToArray());
        keywordRecognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;
        keywordRecognizer.Start();
    }

    void OnDestroy()
    {
        keywordRecognizer.Dispose();
    }

    private void KeywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        KeywordAction keywordAction;

        if (keywordCollection.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke(args);
        }
    }

    // MOVE
    private void MoveCubeCommand(PhraseRecognizedEventArgs args)
    {
        Debug.Log("You said Move.");
        GestureManager.Instance.Transition(GestureManager.Instance.ManipulationRecognizer);
        GestureManager.Instance.isMove = true;
    }

    // SCALE
    //private void ScaleCubeCommand(PhraseRecognizedEventArgs args)
    //{
    //    Debug.Log("You said Hello.");
    //    GestureManager.Instance.Transition(GestureManager.Instance.ManipulationRecognizer);
    //    //GestureManager.Instance.isScale = true;
    //}

    public void Update()
    {
        
    }
}