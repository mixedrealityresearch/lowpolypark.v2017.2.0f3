﻿using HoloToolkit.Unity;
using UnityEngine;

/// <summary>
/// GestureAction performs custom actions based on
/// which gesture is being performed.
/// </summary>
public class ScaleAction : MonoBehaviour
{
    [Tooltip("Rotation max speed controls amount of rotation.")]
    public float ScaleSpeed = 0.1f;

    private Vector3 manipulationPreviousPosition;

    private float rotationFactor;

    void PerformManipulationStart(Vector3 position)
    {
        manipulationPreviousPosition = position;
    }

    void ScaleManipulationStart(Vector3 position)
    {
        manipulationPreviousPosition = position;
    }

    void PerformManipulationUpdate(Vector3 position)
    {
        if (GestureManager.Instance.IsManipulating)
        {
            /* TODO: DEVELOPER CODING EXERCISE 4.a */

            //Vector3 moveVector = Vector3.zero;

            // 4.a: Calculate the moveVector as position - manipulationPreviousPosition.
            //moveVector = position - manipulationPreviousPosition;

            // 4.a: Update the manipulationPreviousPosition with the current position.
            //manipulationPreviousPosition = position;

            // 4.a: Increment this transform's position by the moveVector.
            //transform.position += moveVector;

            transform.localScale += new Vector3(0.1f * ScaleSpeed, 0, 0);
        }
    }
}