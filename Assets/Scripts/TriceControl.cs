﻿using HoloToolkit.Unity;
using System.Collections;
using UnityEngine;
using UnityEngine.Windows.Speech;

//[RequireComponent(typeof(AudioSource), typeof(MicrophoneManager), typeof(KeywordManager))]
public class TriceControl : MonoBehaviour
{
    public ARAI_IntelligenManager IntelligentManager;

    [Tooltip("Drag the Communicator prefab asset.")]
    public GameObject CommunicatorPrefab;
    private GameObject communicatorGameObject;

    [Tooltip("Drag the Voice Tooltip prefab asset.")]
    public GameObject OpenCommunicatorTooltip;
    private GameObject openCommunicatorTooltipGameObject;

    private KeywordManager keywordManager;
    private MicrophoneManager microphoneManager;

    void Awake()
    {
        // Tooltip
        openCommunicatorTooltipGameObject = Instantiate(OpenCommunicatorTooltip);
        openCommunicatorTooltipGameObject.transform.position = new Vector3(
            gameObject.transform.position.x - 1.0f,
            gameObject.transform.position.y + 0.8f,
            gameObject.transform.position.z - 0.8f);

        openCommunicatorTooltipGameObject.transform.parent = gameObject.transform;
        openCommunicatorTooltipGameObject.SetActive(false);

        // keyword manager
        keywordManager = GetComponent<KeywordManager>();

    }

    // Use this for initialization
    void Start()
    {
        microphoneManager = GetComponent<MicrophoneManager>();
    }

    void Update()
    {
        // Make character repeats right after user finishes commands
        if (null != communicatorGameObject)
        {
            ARAIRepeat();
        }
    }

    void GazeEntered()
    {
        //Start keyword recognizer to start giving commands
        keywordManager.StartKeywordRecognizer();
    }

    // When users says 'Hey, ARAI', it will starts recording
    // in 5 seconds (changes this timeout in MicrophoneManager)
    public void StartRecording()
    {
        Debug.Log("Start recording with: " + this.gameObject.transform.name);
        communicatorGameObject = Instantiate(CommunicatorPrefab);
    }

    // ARAI character repeats after user finishs the commands
    // Each command takes 5 seconds in length.
    // We can change this timeout in MicrophoneManager
    void ARAIRepeat()
    {
        Debug.Log("Enter the Repeat function.");
        if (!communicatorGameObject.GetComponent<MicrophoneManager>().hasRecordingStarted)
        {
            Debug.Log("The text: " + communicatorGameObject.GetComponent<MicrophoneManager>().DictationDisplay.text);
            SetTalkARAI(communicatorGameObject.GetComponent<MicrophoneManager>().DictationDisplay.text);

            // Reset communicator gameobject after character repeats
            communicatorGameObject = null;
        }
    }

    void GazeExited()
    {
        // Hide tooltip when user looks away.
        //openCommunicatorTooltipGameObject.SetActive(false);

        // Stop keyword recognizer
        keywordManager.StopKeywordRecognizer();

        communicatorGameObject = null;

        // Reset tooltip to its original state.
        //openCommunicatorTooltipGameObject.GetComponent<VoiceTooltip>().ResetTooltip();
    }

    public void SetTalkARAI(string _text)
    {
        Debug.Log("Enter the SetTalkARAI function. The text is: " + _text);
        //IntelligentManager.intelligentTalkGazedObject(this.gameObject, _text);

        Debug.Log("this gameobject: " + this.gameObject);
        Debug.Log("gaze object: " + GazeManager.Instance.HitInfo.collider.gameObject);

        if (GazeManager.Instance.Hit && GazeManager.Instance.HitInfo.collider.gameObject.transform.tag == "dinosaur")
        {
            IntelligentManager.intelligentTalkGazedObject(GazeManager.Instance.HitInfo.collider.gameObject, _text);
        }
    }
}
