﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARAI_TalkDictionary : MonoBehaviour
{
    public List<TalkAction> talkActionMapper = new List<TalkAction>
    {
        new TalkAction("Hi"         , new string[]{ "안녕" }),
        new TalkAction("ByeBye"     , new string[]{ "잘가" }),
        new TalkAction("Yaw"        , new string[]{ "어디" }),
        new TalkAction("Angry"      , new string[]{ "화나", "화났" }),
        new TalkAction("Bow"        , new string[]{ "슬퍼" }),
    };



    public TalkAction FindTalkAction(string _actionName)
    {
        return talkActionMapper.Find((a) => (a.actionName == _actionName));
    }
}
