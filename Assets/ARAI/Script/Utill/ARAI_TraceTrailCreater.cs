﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARAI_TraceTrailCreater : MonoBehaviour
{
    public Camera MainCamera;
    public GameObject TargetObject;
    public TrailRenderer HelperTrail;

    private bool NowInvisible = true;



    private void OnBecameInvisible()
    {
        NowInvisible = true;
        Debug.Log("Invisible");
    }

    private void OnBecameVisible()
    {
        NowInvisible = false;
        Debug.Log("visible");
    }

    private void Start()
    {
        InvokeRepeating("CreateTraceHelper", 0.5f, 3.0f);
    }

    private void CreateTraceHelper()
    {
        if (NowInvisible)
        {
            TrailRenderer helperTrail = Instantiate(HelperTrail, MainCamera.transform.position /*+ (Vector3.left * 3.0f)*/, MainCamera.transform.rotation);
            
            helperTrail.GetComponent<ARAI_TraceTrail>().TargetTransform = transform;
            helperTrail.GetComponent<ARAI_TraceTrail>().TragetPosition = transform.position;
        }
    }
}
