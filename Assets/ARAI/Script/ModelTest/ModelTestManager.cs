﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModelTestManager : MonoBehaviour
{
    public List<ARAI_IntelligenEntity> sortedIntelligenEntitys;
    public Transform camPivot;
    public Text currentIndexText;
    public Text avatarNameText;
    public Button[] animBtns;
    public GameObject emotionBtnGroup;

    private Camera cam;
    private Transform camTarget;
    private Vector3 camPos = new Vector3(0f, 0f, -2f);
    private Vector3 pivotEulerAngles;
    private Vector3 mouseLastPos;
    private ARAI_IntelligenManager intelligentManager;
    private int avatarIndex = 0;
    private int avatarMaxIndex = 0;
    private bool isAvatar = true;
    


	private void Start()
    {
        cam = Camera.main;

        intelligentManager = GameObject.FindObjectOfType<ARAI_IntelligenManager>();
        intelligentManager.intelligentEntitys = sortedIntelligenEntitys;
        intelligentManager.SwapIntelligentEntity(avatarIndex);
        avatarMaxIndex = intelligentManager.intelligentEntitys.Count - 1;
        camTarget = intelligentManager.intelligentEntity.headTransform;
     
        foreach (var entity in intelligentManager.intelligentEntitys)
        {
            entity.transform.position = Vector3.zero;
            if (entity != intelligentManager.intelligentEntity)
                entity.gameObject.SetActive(false);
        }

        UpdateAvatarName();
        UpdateEmotionBtns();
        UpdateAnimationBtns();
    }

    private void Update()
    {
        UpdateCamera();
    }

    private void UpdateCamera()
    {
        float wheel = Input.GetAxis("Mouse ScrollWheel");
        camPos.z = Mathf.Clamp(camPos.z + wheel * 3.0f, -15f, -0.5f);
        cam.transform.localPosition = camPos;

        if (Input.GetMouseButton(0))
        {
            if (Input.GetMouseButtonDown(0))
                mouseLastPos = Input.mousePosition;
            else
            {
                Vector3 delta = Input.mousePosition - mouseLastPos;
                pivotEulerAngles.x -= delta.y * 0.1f;
                pivotEulerAngles.y += delta.x * 0.1f;

                camPivot.eulerAngles = pivotEulerAngles;
                mouseLastPos = Input.mousePosition;
            }
        }
        camPivot.position = camTarget.position;

        cam.transform.LookAt(camTarget);
    }

    private void UpdateAnimationBtns()
    {
        ARAI_TalkDictionary talkDic = intelligentManager.intelligentEntity.talkDictionary;

        for (int i = 0; i < 8; ++i)
        {
            if (i < talkDic.talkActionMapper.Count)
            {
                animBtns[i].gameObject.SetActive(true);
                animBtns[i].GetComponentInChildren<Text>().text = talkDic.talkActionMapper[i].actionName;
            }
            else
            {
                animBtns[i].gameObject.SetActive(false);
            }
        }
    }

    private void UpdateAvatarName()
    {
        string objName = intelligentManager.intelligentEntity.name;
        string avatarName = objName.Substring(objName.IndexOf('_') + 1);
        avatarNameText.text = avatarName;
        isAvatar = objName.StartsWith("Avatar");
    }

    private void UpdateEmotionBtns()
    {
        emotionBtnGroup.SetActive(isAvatar);
    }

    public void OnChangeAvatar(int _moveIndex)
    {
        if ((avatarIndex == 0 && _moveIndex < 0) ||
            (avatarIndex == avatarMaxIndex && _moveIndex > 0))
            return;

        avatarIndex += _moveIndex;
        currentIndexText.text = avatarIndex.ToString("D02");

        intelligentManager.intelligentEntity.gameObject.SetActive(false);
        intelligentManager.SwapIntelligentEntity(avatarIndex);
        intelligentManager.intelligentEntity.gameObject.SetActive(true);

        camTarget = intelligentManager.intelligentEntity.headTransform;
        SkinnedMeshRenderer skin = intelligentManager.intelligentEntity.skinnedMesh;
        float range = skin.bounds.extents.magnitude;
        if (range < 1f)
            range = 1f;
        camPos.z = -range;

        UpdateAvatarName();
        UpdateEmotionBtns();
        UpdateAnimationBtns();
    }

    public void OnPlayAnimation(int _btnIndex)
    {
        string actionName = animBtns[_btnIndex].GetComponentInChildren<Text>().text;
        intelligentManager.intelligentEntity.animator.Play(actionName, -1, 0f);
    }

    public void OnPlayEmotion(string _emotionName)
    {
        intelligentManager.ResetEmotion();
        intelligentManager.PlayEmotion(_emotionName, 1.0f);
    }
}
