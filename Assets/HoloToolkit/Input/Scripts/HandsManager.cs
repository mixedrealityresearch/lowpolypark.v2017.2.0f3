﻿
using UnityEngine;

//namespace Academy.HoloToolkit.Unity
namespace HoloToolkit.Unity
{
    /// <summary>
    /// HandsManager keeps track of when a hand is detected.
    /// </summary>
    public class HandsManager : Singleton<HandsManager>
    {
        [Tooltip("Audio clip to play when Finger Pressed.")]
        public AudioClip FingerPressedSound;
        private AudioSource audioSource;

        /// <summary>
        /// Tracks the hand detected state.
        /// </summary>
        public bool HandDetected
        {
            get;
            private set;
        }

        // Keeps track of the GameObject that the hand is interacting with.
        public GameObject FocusedGameObject { get; private set; }

        void Awake()
        {
            EnableAudioHapticFeedback();

            UnityEngine.XR.WSA.Input.InteractionManager.InteractionSourceDetectedLegacy += InteractionManager_SourceDetected;
            UnityEngine.XR.WSA.Input.InteractionManager.InteractionSourceLostLegacy += InteractionManager_SourceLost;

            UnityEngine.XR.WSA.Input.InteractionManager.InteractionSourcePressedLegacy += InteractionManager_SourcePressed;
            UnityEngine.XR.WSA.Input.InteractionManager.InteractionSourceReleasedLegacy += InteractionManager_SourceReleased;

            FocusedGameObject = null;
        }

        private void EnableAudioHapticFeedback()
        {
            // If this hologram has an audio clip, add an AudioSource with this clip.
            if (FingerPressedSound != null)
            {
                audioSource = GetComponent<AudioSource>();
                if (audioSource == null)
                {
                    audioSource = gameObject.AddComponent<AudioSource>();
                }

                audioSource.clip = FingerPressedSound;
                audioSource.playOnAwake = false;
                audioSource.spatialBlend = 1;
                audioSource.dopplerLevel = 0;
            }
        }

        private void InteractionManager_SourceDetected(UnityEngine.XR.WSA.Input.InteractionSourceState hand)
        {
            HandDetected = true;
        }

        private void InteractionManager_SourceLost(UnityEngine.XR.WSA.Input.InteractionSourceState hand)
        {
            HandDetected = false;

            ResetFocusedGameObject();
        }

        private void InteractionManager_SourcePressed(UnityEngine.XR.WSA.Input.InteractionSourceState hand)
        {
            if (InteractibleManager.Instance.FocusedGameObject != null)
            {
                // Play a select sound if we have an audio source and are not targeting an asset with a select sound.
                if (audioSource != null && !audioSource.isPlaying &&
                    (InteractibleManager.Instance.FocusedGameObject.GetComponent<Interactible>() != null &&
                    InteractibleManager.Instance.FocusedGameObject.GetComponent<Interactible>().TargetFeedbackSound == null))
                {
                    audioSource.Play();
                }

                FocusedGameObject = InteractibleManager.Instance.FocusedGameObject;
            }
        }

        private void InteractionManager_SourceReleased(UnityEngine.XR.WSA.Input.InteractionSourceState hand)
        {
            ResetFocusedGameObject();
        }

        private void ResetFocusedGameObject()
        {
            FocusedGameObject = null;

            GestureManager.Instance.ResetGestureRecognizers();
        }

        void OnDestroy()
        {
            UnityEngine.XR.WSA.Input.InteractionManager.InteractionSourceDetectedLegacy -= InteractionManager_SourceDetected;
            UnityEngine.XR.WSA.Input.InteractionManager.InteractionSourceLostLegacy -= InteractionManager_SourceLost;

            UnityEngine.XR.WSA.Input.InteractionManager.InteractionSourceReleasedLegacy -= InteractionManager_SourceReleased;
            UnityEngine.XR.WSA.Input.InteractionManager.InteractionSourceReleasedLegacy -= InteractionManager_SourcePressed;
        }
    }
}