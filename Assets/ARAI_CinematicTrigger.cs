﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARAI_CinematicTrigger : MonoBehaviour {

    public GameObject CinematicObject;
    public ARAI_IntelligenEntity AvatarObject;

    private ARAI_IntelligenManager IntelligentManager;

	// Use this for initialization
	void Start () {
        CinematicObject.active = false;
        AvatarObject.gameObject.active = false;

        IntelligentManager = GameObject.FindObjectOfType<ARAI_IntelligenManager>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<Camera>())
        {
            CinematicObject.active = true;
            AvatarObject.gameObject.active = true;

            IntelligentManager.SetIntelligentEntity(AvatarObject);
        }
        
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Camera>())
        {
            CinematicObject.active = false;
            AvatarObject.gameObject.active = false;
        }
    }
}
